const express = require('express')
const session = require('express-session')
const bodyParser = require('body-parser')
const next = require('next')
const mongoose = require('./libs/mongoose')
const mongoStore = require('connect-mongo')(session)

const dev = process.env.NODE_ENV !== 'production'
const app = next ({dev})
const handle = app.getRequestHandler()
const server = express()

app.prepare()
.then(()=>{
    server.use(require('express-session')({
        secret:'keyboard cat', 
        resave: false, 
        saveUninitialized: true,
        store: new mongoStore ({mongooseConnection : mongoose.connection, clear_interval: 60 * 60 * 24})
      }))
    server.get('*',(req, res) =>{
        return handle(req,res)
    })
    server.listen(3000, (err) => {
        if (err) throw err
        console.log('>Ready')
    })
})


.catch ((ex) =>{
    console.error(ex.stack)
    process.exit(1)
})