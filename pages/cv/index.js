import Layout from '@/components/layout';
import TrixEditor from '@/components/TrixInput/TrixInputNoSSR'

export default ()=>{
    return <Layout>
        <TrixEditor/>
    </Layout>
}