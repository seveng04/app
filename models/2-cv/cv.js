const mongoose = require('../../libs/mongoose')
const Schema = mongoose.Schema

const cvSchema = Schema({
    title : {
        type : String,
        required : true
    },
    body : {
        type : Array,
        required : true
    }
})

exports.cvSchema = mongoose.model('CvSchema', cvSchema)