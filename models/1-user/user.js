const mongoose = require('../../libs/mongoose')
Schema  = mongoose.Schema

const userSchema = Schema({
    userId : {
        type : String,
        required: true,
        unique : true
    },
    login : {
        type : String,
        required : true
    },
    password: {
        type : String
    }
})

userSchema.methods.comparePassword = (password) =>{
    return password === this.password
}

exports.User = mongoose.model('User', userSchema)