import React, {useState} from 'react'
import Dante from 'Dante2'
import style from '@/components/DanteEditor.module.scss'

export default function DanteEditor(){
    // const [value, setValue] = useState("asd");
    // console.log(value);
    return (
    <div id={style.biba}>
        <Dante onChange={(editor) => { console.log('editor content: ', editor.emitSerializedOutput()) }}/>
    </div>)
} 