import Link from 'next/link'

const NavPanel = () => {return(
    <ul>
        <li>
        <Link href='/'>
            <a>Home</a>
        </Link>
        </li>
        <li>
        <Link href='/1/1'>
            <a>1/1</a>
        </Link>
        </li>
        <li>
        <Link href='/cv'>
            <a>cv</a>
        </Link>
        </li>
        <li>
        <Link href='/componentAdder'>
            <a>componentAdder</a>
        </Link>
        </li>
    </ul>
)}
export default NavPanel