import NavPanel from "@/components/NavPanel"
function Layout({ children }) {
    return <div>
        <NavPanel/>
        {children}
      </div>
  }
  export default Layout
  