import dynamic from 'next/dynamic'

const TrixInputNoSSR = dynamic(() => import('@/components/TrixInput/TrixInput'), {
  ssr: false
})

export default () => <TrixInputNoSSR />