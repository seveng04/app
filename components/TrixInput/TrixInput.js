import Trix from "trix";
import React, { useState } from "react";
import { ReactTrixRTEInput } from "react-trix-rte";
import style from '@/styles/components/TrixInput.module.scss'

export default function TrixEditor(props) {
  const [value, setValue] = useState("");

  function handleChange(event, newValue) {
    console.log(newValue);
    setValue(newValue); // OR custom on change listener.
  }

  return (
    <div id={style.trixEditor}>
        <ReactTrixRTEInput
          defaultValue="<div>React Trix Rich Text Editor</div>"
          onChange={handleChange}
        />
    </div>
  )
}