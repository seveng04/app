const mongoose = require('mongoose')
const config = require('config')

mongoose.connect(config.get('mongoose.uri'))
    .then (()=> console.log('db connection ok'))
    .catch(()=> console.log('db connection failed'))

module.exports = mongoose